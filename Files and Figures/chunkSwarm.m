function chunkSwarm(chunk, nochunk)

addpath(fullfile(pwd, 'plotSpread'))

data = [chunk(:,2); nochunk(:,2)];
catIdx = [zeros(1, 100), ones(1, 100)];
figure(); plotSpread(data, 'distributionIdx', [chunk(:,1); nochunk(:,1)], ...
    'categoryIdx', catIdx, 'categoryLabels', {'Sequences', 'No Sequences'}, ...
    'categoryMarkers',{'o','+'}, 'ylabel', 'Processing Per Step'); legend('show')
set(gca, 'FontSize', 20); set(findall(gcf,'type','text'), 'FontSize', 20)
set(findobj(gca, 'type', 'line'),'linew', 1.3)
ylim([0 inf]);

end