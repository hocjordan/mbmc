# Neural Network for Model-Based Motor Control

## Code Structure

The code is initialised using MBMC_master(). This function reads and sets parameter values from a .txt file in the Experiments folder, and interfaces with the Dakota optimisation software if necessary. MBMC_master creates the basic world structure and calls a wrapper which creates the agent and performs the designated experiment.

## To Create an Agent

'GatingColWTA.txt' is the most reliable learning procedure, and can be called as follows:

[results, agent, world, switches] = MBMC_master('GatingColWTA', {}, [], []);

will learn a simple environment. The agent moves around randomly and self-organises all of the relevant cell types and connectivity, becoming ready to do tasks.

## To Perform Tasks

[results, ~, ~, switches] = MBMC_master('ControllerTest', {}, agent, world);

will perform 100 navigation tasks in the environment contained by 'world', using the agent contained in 'agent'. In each task, a random state is set as the agent’s starting location and another random state is set as the agent’s goal. If the agent navigates to the goal within a certain number of time steps then the task is successful. High-level actions are learned at this point.

## Results Analysis

After a simulation, MBMC_master() will output a 'results' structure, containing various forms of data including task performance and recorded cell firing. The contents of the 'results' structure can be determined using the experiment .txt files.

slider_display(results.sensory_tracked{N}, [], true);

will show the agent’s movement on task N.

slider_display(results.SA_tracked{N}, [], true);

will show the activity of the agent’s SA cells on task N.

The diagnostics folder contains more sophisticated analyses.