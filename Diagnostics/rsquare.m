function rsq_adj = rsquare(fitvar, x, y)

yfit = fitvar(x);

yresid = y - yfit;

SSresid = sum(yresid.^2);

SStotal = (length(y)-1) * var(y);

rsq = 1 - SSresid/SStotal;

rsq_adj = 1 - SSresid/SStotal * (length(y)-1)/(length(y)-length(coeffvalues(fitvar)));

end