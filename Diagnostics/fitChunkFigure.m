function fitChunkFigure()
datachild = get(gca, 'Children')
fit(datachild(2).XData', datachild(2).YData', 'poly1', 'Weights', datachild(2).SizeData')
fit(datachild(1).XData', datachild(1).YData', 'poly2', 'Weights', datachild(1).SizeData')
end