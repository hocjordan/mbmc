function FullMartinetScript()

ChunkPath

simulatedAnimals = 40;

occupancy_A = cell([1 simulatedAnimals]);
occupancy_B = cell([1 simulatedAnimals]);
occupancy_Open = cell([1 simulatedAnimals]);

sensory_tracked_A = cell([1 simulatedAnimals]);
sensory_tracked_B = cell([1 simulatedAnimals]);
sensory_tracked_Open = cell([1 simulatedAnimals]);


savepath = fullfile('Results', 'MartinetResults');
if ~exist(savepath, 'dir')
    mkdir(savepath)
end

for i = 1:simulatedAnimals
[occupancy_tmp, sensory_tracked_tmp, ~, ~] = MartinetScript();
occupancy_Open{i} = occupancy_tmp.Open; occupancy_A{i} = occupancy_tmp.A; occupancy_B{i} = occupancy_tmp.B;
sensory_tracked_Open{i} = sensory_tracked_tmp.Open; sensory_tracked_A{i} = sensory_tracked_tmp.A; sensory_tracked_B{i} = sensory_tracked_tmp.B;

save(fullfile(savepath, sprintf('Occupancy_%d', i)), 'occupancy_tmp')
save(fullfile(savepath, sprintf('sensory_tracked_%d', i)), 'sensory_tracked_tmp')

end

save FullMartinetResults

end