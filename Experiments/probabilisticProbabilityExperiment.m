function probabilisticProbabilityExperiment()

[results, agent, world, switches] = MBMC_master('GatingColWTA', {}, [], []);
norm_agent = agent;
alt_agent = agent;
cells = SA_query(agent.SA_decoded, 'state', 48);
cell_readout = cells(find(cells(:,2) == 8), :);
assert(size(cell_readout, 1) == 1)
snipped_cell = cell_readout(3);
alt_agent.SAtoSA_synapses(:,snipped_cell) = alt_agent.SAtoSA_synapses(:,snipped_cell) * 0.1;
[alt_results, ~, world, switches] = MBMC_master('ProbPropTest', {}, alt_agent, world);
altOccupancy = makeOccupancyGrid(alt_results.occupancy, false, 'Edited Probabilities');
[norm_results, ~, world, switches] = MBMC_master('ProbPropTest', {}, norm_agent, world);
normOccupancy = makeOccupancyGrid(norm_results.occupancy, false, 'Normal Probabilities');
tmp1 = normOccupancy(48)/(normOccupancy(48)+normOccupancy(44));
tmp2 = altOccupancy(48)/(altOccupancy(48)+altOccupancy(44));
figure(); bar([tmp1 tmp2])
ylim([0 1])

end