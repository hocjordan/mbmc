function comparisonFigure(output)
figure(); 

subplot(2,2,1);
chunkRatioFig(output.so_chunk, output.so_nochunk); title('Small Open')
subplot(2,2,2); 
chunkRatioFig(output.sm_chunk, output.sm_nochunk); title('Small Maze')
subplot(2,2,3); 
chunkRatioFig(output.lo_chunk, output.lo_nochunk); title('Large Open')
subplot(2,2,4); 
chunkRatioFig(output.lm_chunk, output.lm_nochunk); title('Large Maze')
end