function chunkRatioFig(chunk, nochunk)
hold on
scatter(chunk(:,1), chunk(:,2), chunk(:,3)*10, 'filled');
ax_tmp = scatter(nochunk(:,1), nochunk(:,2), nochunk(:,3)*10, 'd', 'filled');
xlabel('Route Length (steps)'); ylabel('Total Planning Time (timesteps)');
set(gca, 'FontSize', 30);
[h,icons,plots,legend_text] = legend('Augmented', 'Basic');
icons(3).Children.MarkerSize = 10; icons(4).Children.MarkerSize = 10;
hold off
end