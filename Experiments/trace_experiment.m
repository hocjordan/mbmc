function trace_experiment()

layer1 = zeros([1 5]);
layer2 = zeros([1 5]);
layer1(1) = 1;
layer2(3) = 1;

matrix_delta = layer1(:) * layer2(:)';

for i = 1:numel(layer1)
    for j = 1:numel(layer2)
        loop_delta(i, j) = layer1(i) * layer2(j);
    end
end

matrix_recur = layer1(:) * layer1(:)';

for i = 1:numel(layer1)
    for j = 1:numel(layer1)
        loop_recur(i, j) = layer1(i) * layer1(j);
    end
end

end