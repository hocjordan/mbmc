% Get Agent and Chunks
[results, agent, world, switches] = MBMC_master('GatingColWTA', {}, [], []);
[results, agent_chunks, ~, switches] = MBMC_master('LearningChunks', {}, agent, world);

% Add Chunk Synapses Together
add_chunks = zeros(size(agent_chunks.chunktoSA_synapses(1,:)));
for i = 1:numel(agent_chunks.chunk_cells)
add_chunks = add_chunks + agent_chunks.chunktoSA_synapses(i,:);
end


agent_copy = agent_chunks;
agent_copy.chunktoSA_synapses(1, :) = add_chunks;
chunk_analyseSynCompass(agent_copy, world, 1, 'from', true, true)

agent_copy.chunktoSA_synapses(1,:) = agent_copy.chunktoSA_synapses(1,:) / max(max(agent_copy.chunktoSA_synapses(1,:) ));
agent_copy.chunktoSA_synapses(agent_copy.chunktoSA_synapses > 0.1) = 1;
chunk_analyseSynCompass(agent_copy, world, 1, 'from', true, true)