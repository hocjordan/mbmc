function world = load_walls(world, switches)
assert(isequal(switches.main.worldType, 'maze'))

switch switches.params.walls
    case 'SmallOpen'
        tmp_walls = load(fullfile(pwd, 'Walls', 'smallopen_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'SmallMaze'
        tmp_walls = load(fullfile(pwd, 'Walls', 'smallmaze_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'SmallBisected'
        tmp_walls = load(fullfile(pwd, 'Walls', 'smallbisected_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'LargeOpen'
        tmp_walls = load(fullfile(pwd, 'Walls', 'open_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'LargeMaze'
        tmp_walls = load(fullfile(pwd, 'Walls', 'maze_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'Fakhari1'
        tmp_walls = load(fullfile(pwd, 'Walls', 'Fakhari1_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    case 'SmallTwoGate'
        tmp_walls = load(fullfile(pwd, 'Walls', 'twogate_walls.mat'), 'walls');
        world.walls = tmp_walls.walls;
    otherwise
        error('Invalid Walls Specified')
end

if isfield(world, 'walls')
    if ~switches.main.suppressChecks
        if ~isequal(world.walls, tmp_walls.walls)
            if ~isequal(input('WARNING: Overwrite Map? ', 's'),'Y')
                error('Bad Mode')
            end
        end
    end
end

assert(switches.main.manual_walls == false)
end