%% Martinet Script
function [occupancy, sensory_tracked, world, agent] = MartinetScript()

%% Setup
occupancy = struct();
sensory_tracked = struct();
occupancy.A = {}; 
occupancy.B = {};
occupancy.Open = {};
sensory_tracked.A = {};
sensory_tracked.B = {};
sensory_tracked.Open = {};
global goalIterations
goalIterations = 0;
clearvars -global firing

ChunkPath

global martinetExperiment
martinetExperiment = 'Open'; % 'AGate' 'A' 'BGate' 'B' 'Open' 'P1' P2 P3

%% Day 1
% 3 forced runs where simulated rats are forced through P1, P2 & P3.
load('MartinetWallsOpen.mat')

assert(numel(world.actions) == 5);
assert(numel(agent.motor_cells) == 5);

martinetExperiment = 'P1';
[~, ~, agent, ~] = ChunkMaze_World('maze', true, false, agent, world.walls);
martinetExperiment = 'P2';
[~, ~, agent, ~] = ChunkMaze_World('maze', true, false, agent, world.walls);
martinetExperiment = 'P3';
[~, ~, agent, ~] = ChunkMaze_World('maze', true, false, agent, world.walls);

% 9 trials of free exploration.
%martinetExperiment = 'Open';
%trials = 9;
%{
for i = 1:trials
    [~, ~, agent, ~] = ChunkMaze_World('maze', true, false, agent, world.walls);
end
%}
martinetExperiment = 'Open';
goalIterations = 9;
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.Open = horzcat(occupancy.Open, occupancy_Open_tmp);
    sensory_tracked.Open = horzcat(sensory_tracked.Open, sensory_tracked_tmp);

%% Days 2 - 14
% Loop: 12 trials per day; P1 takes are *blocked* and so should not be
% counted for anything
for i = 1:13
    
    % 10 trials with Location A Blocked
    taskIdx = zeros([1 2]);
    while taskIdx(1) == taskIdx(2)
        taskIdx = sort(randi(12, [1 2]));
    end
    load('MartinetWallsBlockAGate.mat');
    martinetExperiment = 'AGate';
    goalIterations = taskIdx(1) - 1;
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.A = horzcat(occupancy.A, occupancy_A_tmp);
    sensory_tracked.A = horzcat(sensory_tracked.A, sensory_tracked_tmp);
    
    load('MartinetWallsOpenGate', 'world');
    martinetExperiment = 'OpenGate';
    goalIterations = 1;
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.Open = horzcat(occupancy.Open, occupancy_Open_tmp);
    sensory_tracked.Open = horzcat(sensory_tracked.Open, sensory_tracked_tmp);
    
    load('MartinetWallsBlockAGate.mat');
    martinetExperiment = 'AGate';
    goalIterations = taskIdx(2) - taskIdx(1) - 1;
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.A = horzcat(occupancy.A, occupancy_A_tmp);
    sensory_tracked.A = horzcat(sensory_tracked.A, sensory_tracked_tmp);
    
    load('MartinetWallsOpenGate', 'world');
    martinetExperiment = 'OpenGate';
    goalIterations = 1;
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.Open = horzcat(occupancy.Open, occupancy_Open_tmp);
    sensory_tracked.Open = horzcat(sensory_tracked.Open, sensory_tracked_tmp);
    
    load('MartinetWallsBlockAGate.mat');
    goalIterations = 12 - taskIdx(2);
    martinetExperiment = 'AGate';
    [~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
    occupancy.A = horzcat(occupancy.A, occupancy_A_tmp);
    sensory_tracked.A = horzcat(sensory_tracked.A, sensory_tracked_tmp);
end


%% Day 15
% 7 trials with Location B Blocked
load('MartinetWallsBlockBGate')
martinetExperiment = 'BGate';
goalIterations = 7;
clear('ChunkMaze_World')
[~, ~, agent, ~] = ChunkMaze_World('maze', false, true, agent, world.walls);
occupancy.B = horzcat(occupancy.B, occupancy_B_tmp);
sensory_tracked.B{end + 1} = horzcat(sensory_tracked.B, sensory_tracked_tmp);
save tmp

end